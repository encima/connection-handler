#include <string>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
using namespace std;

class ConnectionHandler
{
	public:
		string execCmd(char* cmd) {
			FILE* pipe = popen(cmd, "r");
				if(!pipe) return "Error";
			char buffer[128];
			string result = "";
			while(!feof(pipe)) {
				if(fgets(buffer, 128, pipe) != NULL)
					result += buffer;
			}
			pclose(pipe);
			return result;
		}

		bool getNetworkState() {
			bool net = false;
			char* cmd = (char*)"iwconfig wlan0";
			string test = execCmd(cmd);
			string adHoc = "Ad-Hoc";
				if(string::npos != test.find(adHoc)) {
					net = true;
					cout << "Wireless Interface is in Ad Hoc mode" << endl;
				}else{
					net = false;
					cout << "Wireless Interface is in Managed mode" << endl;
				}
			return net;
		}

		void changeNetworkState(bool net) {
			if(net) {
				cout << "Changing Wireless Interface from Ad Hoc to Managed" << endl;
				system("connectManaged");
			}else{
				cout << "Changing Wireless Interface from Managed to Ad Hoc" << endl;
				system("createAdHoc");
			}
			sleep(5);
			getNetworkState();
		}
};

int main() {
	system("stop network-manager");
	ConnectionHandler ch;
	bool net = false;
	net = ch.getNetworkState();
	ch.changeNetworkState(net);
	return 0;
}
